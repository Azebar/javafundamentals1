package May09;

import java.util.Arrays;

public class Practice2 {

    public static int[] arrayCompare(int[] a, int[] b) {
        int arraySize = 1;
        int array[] = new int[arraySize];

        if(a.length > b.length){
            for (int i = 0; i < b.length; i++) {
                if (a[i] == b[i]) {
                    array[i] = a[i];
                    arraySize++;
                }
            }
        }else{
            for (int i = 1; i < b.length; i++) {
                if (b[i-1] == a[i-1]) {
                    array[i-1] = b[i-1];
                    arraySize++;
                }
            }
        }

        return array;
    }

        public static void main (String[]args){

            int[] arr1 = {1, 2, 4};
            int[] arr2 = {2, 5, 6, 4};
            int[] arr3 = arrayCompare(arr1, arr2);

            System.out.println(Arrays.toString(arr1));
            System.out.println(Arrays.toString(arr2));
            System.out.println(Arrays.toString(arr3));

        }


}
