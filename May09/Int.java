package May09;

public class Int {

    public int val;

    public Int(int val){
        this.val = val;
    }

    @Override
    public String toString() {
        return "Int{" +
                "val=" + val +
                '}';
    }
}
