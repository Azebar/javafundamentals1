package May09;

public class PracticeFunction {

    private static int addOne(int a){
        System.out.println("addOne: " + a);
        a = a+1;
        System.out.println("addOne: " + a);
        return a;

    }

    public static void example1(){
        int a =7;
        System.out.println(a);

        int returnOfAddOne = addOne(a);
        System.out.println("returnOfAddOne: " + returnOfAddOne);

        a = a + returnOfAddOne;
        a = addOne(a);
        a = addOne(a);
        System.out.println(a);
    }

    private static void addOne(Int a){
        System.out.println("addOne: " + a);
        //a.val = a.val + 1;
        int b = a.val +1;
        //a = new Int(b);
        //a = new Int(7);
        a.val = b;
        System.out.println("addOne: " + a);

    }

    public static void example2(){

        Int a = new Int(7);
        System.out.println(a);
        System.out.println(a.val);

        addOne(a);
        System.out.println(a);
        System.out.println(a.val);

        a.val = 7;
        addOne(a.val);
        System.out.println(a.val);
    }

    public static void main(String[] args) {

        //example1();
        example2();

    }
}
