package May09;

public class Practice1 {

    public static int getElementNumber(int n){

      int el1 = 1;
      int el2 = 1;

        for(int i = 2; i < n;i++){

            int newEl = el2 + el1;
            el1 = el2;
            el2 = newEl;


        }

        return el2;

    }
    //n50 = n49 + n48
    //n49 = n48 + n47
    //n48

    public static int calculateFibonacci(int n) {

        if(n == 1){return 1;}
        if(n == 2){return 1;}

        int n1 = calculateFibonacci(n-1);
        int n2 = calculateFibonacci(n-2);
        return n1+n2;

    }



    public static void main(String[] args) {
        calculateFibonacci(4);
        for(int i = 1; i < 20; i++) {
            System.out.println(i + "th element is: " + calculateFibonacci(i));
        }

    }

}
