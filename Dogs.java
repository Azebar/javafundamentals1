public class Dogs {

    private int age;
    private String name;
    private String Breed;

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public String getBreed() {
        return Breed;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setBreed(String Breed) {
        this.Breed = Breed;
    }

    public void Talk(){
       switch (Breed){
           case "Golden Retriever":
               System.out.println("Guf-Guf");
               break;
           case "Bloodhound":
               System.out.println("Buf-Buf");
               break;
           case "Border Terrier":
               System.out.println("Tuf-Tuf");
               break;
           case "Border Collie":
               System.out.println("Cuf-Cuf");
               break;
           case "West Highland White Terrier":
               System.out.println("Wuf-Wuf");
               break;
        }
    }

    public static void main(String[] args) {

        Dogs dog1 = new Dogs();
        dog1.setName("Hugo");
        dog1.setAge(12);
        dog1.setBreed("Golden Retriever");
        Dogs dog2 = new Dogs();
        dog2.setName("Tom");
        dog2.setAge(8);
        dog2.setBreed("Bloodhound");
        Dogs dog3 = new Dogs();
        dog3.setName("Jerry");
        dog3.setAge(6);
        dog3.setBreed("Border Terrier");
        Dogs dog4 = new Dogs();
        dog4.setName("Pluto");
        dog4.setAge(9);
        dog4.setBreed("Border Collie");
        Dogs dog5 = new Dogs();
        dog5.setName("Youngster");
        dog5.setAge(3);
        dog5.setBreed("West Highland White Terrier");
        System.out.println(dog1.name);
        System.out.println(dog1.age);
        System.out.println(dog1.Breed);
        dog1.Talk();
        System.out.println(dog2.name);
        System.out.println(dog2.age);
        System.out.println(dog2.Breed);
        dog2.Talk();
        System.out.println(dog3.name);
        System.out.println(dog3.age);
        System.out.println(dog3.Breed);
        dog3.Talk();
        System.out.println(dog4.name);
        System.out.println(dog4.age);
        System.out.println(dog4.Breed);
        dog4.Talk();
        System.out.println(dog5.name);
        System.out.println(dog5.age);
        System.out.println(dog5.Breed);
        dog5.Talk();
    }

}
