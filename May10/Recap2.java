package May10;

import java.util.Arrays;
import java.util.Scanner;

public class Recap2 {

    private static boolean isInArray(String[] array, String el) {

        for (String element : array) {
            if (element.equals(el)) {
                return true;
            }
        }
        return false;
    }

    private static String upCaseWords(String text, String[] words) {

        String[] textWords = text.split(" ");
        int position = 0;
        for (String el : textWords) {
            if (isInArray(words, el)) {
                textWords[position] = textWords[position].toUpperCase();
            }
            position++;
        }

        return Arrays.toString(textWords);

    }

    public static void main(String[] args) {

        System.out.println("Please enter text:");

        Scanner scanner = new Scanner(System.in);
        String userInput = scanner.nextLine();
        String[] words = {"bla", "banana"};
        String upCase = upCaseWords(userInput, words);

        System.out.println(upCase);

    }



}
