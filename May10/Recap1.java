package May10;

import java.util.Scanner;

public class Recap1 {

    //public static Scanner scanner = new Scanner(System.in);

    public static String getCatName(Scanner scanner){
        //Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter cat name:");
        //String catName = scanner.nextLine();
        return scanner.nextLine();
    }

    public static int getCatAge(Scanner scanner){
        //Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter cat age:");
        //int catAge = scanner.nextInt();
        return scanner.nextInt();
    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String cat1 = "fifi";
        String cat2 = "john";
        int catAge1 = 4;
        int catAge2 = 3;

        String catName = getCatName(scanner);
        int catAge = getCatAge(scanner);

        if((catName.equals(cat1) && catAge == catAge1) || (catName.equals(cat2) && catAge == catAge2)){
            System.out.println("we have cat");
        }
        else if(catName.equals(cat1) || catName.equals(cat2)){
            System.out.println("that cat has different age");
        }
        else if(catAge == catAge1 || catAge == catAge2){
                System.out.println("cat that old has different name");
        }
        else{
            System.out.println("no that kind of cats");
        }

    }

}
