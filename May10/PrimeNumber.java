package May10;

import java.util.Scanner;

public class PrimeNumber {

    public static boolean checkPrimeNumber(int n) {

        for (int i = 2; i < n; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {

        Scanner scn = new Scanner(System.in);
        System.out.println("Please enter any number to check if it is a prime number: ");
        int number = scn.nextInt();
        if(checkPrimeNumber(number)) {
            System.out.println(number + " is a prime number");
        } else {
            System.out.println(number + " is not a prime number");
        }

    }

}
