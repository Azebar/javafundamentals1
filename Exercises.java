public class Exercises {

    public static int GetBigger(int a, int b){
        if(a>b){
            return a;
        } else {
            return b;
        }
    }

   /*public static int CheckEven(int a){

        if(a % 2 == 0) {

            System.out.println(a + " is even");

        } else{

            System.out.println(a + " is odd");

         }

    }*/

    public static boolean IsLeapYear(int year){
        if(year % 400 == 0 || year % 4 == 0 && year % 100 != 0){
            return true;
        } else return false;
    }

    public static String FizzBuzz(int a){
        if(a % 3 == 0 && a % 5 == 0) {
            return "fizzbuzz";
        }else if (a % 5 == 0){
            return "buzz";
        }else if (a % 3 == 0){
            return "fizz";
        }else {
            return "No fizzbuzz";
        }

    }

    public static void main(String[] args) {

        //int bigger = GetBigger(2, 4);
        //System.out.println(bigger);
        //int even = CheckEven(11);
        //boolean isLeap = IsLeapYear(2020);
        //System.out.println(isLeap);
        String fuzzbuzz = FizzBuzz(8);
        System.out.println(fuzzbuzz);
        }

}
