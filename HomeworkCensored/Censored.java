package HomeworkCensored;
import java.util.Arrays;


public class Censored {

    private static boolean isInArray(String[] array, String el) {

        for (String element : array) {
            if (element == el) {
                return true;
            }
        }
        return false;

    }

    public static String[] removeCensored(String[] text, String[] censoredWords){

        String[] result = text;
        int position = 0;
        for (String el : text) {
            if (isInArray(censoredWords, el)) {
                result[position] =  "";
            }
            position++;
        }
        return result;

    }

    public static void main(String[] args) {

        String[] text = {"I", "really", "do", "not", "like", "Java"};
        String[] censoredWords = {"banana", "do", "kiwi", "not"};
        String[] result = removeCensored(text, censoredWords);
        System.out.println(Arrays.toString(result));

    }

}
