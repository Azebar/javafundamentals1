package HomeworkCensored;

public class IfElseAndForWithIndexes {

        public static String FizzBuzz(int a){
            if(a % 3 == 0 && a % 5 == 0) {
                return "fizzbuzz";
            }else if (a % 5 == 0){
                return "buzz";
            }else if (a % 3 == 0){
                return "fizz";
            }else {
                return "No fizzbuzz";
            }

        }

        public static void main(String[] args) {
            for(int a = 0; a<101; a++){

                String fizzbuzz = FizzBuzz(a);
                System.out.println(a + " is " + fizzbuzz);

            }

        }

}
