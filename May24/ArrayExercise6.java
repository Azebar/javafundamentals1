package May24;

import java.util.Arrays;

public class ArrayExercise6 {

    public static void main(String[] args) {

        int[] arr = new int[]{-1, -10, 8, 3, 2, -7, -3};
        sortArray(arr,arr.length);
        System.out.println(Arrays.toString(arr));

    }

    public static void sortArray(int arr[], int len){
        int key;
        int j;

        for (int i = 0; i < len; i++){
            key = arr[i];

            if(key < 0){
                continue;
            }

            j = i - 1;
            while (j >= 0 && arr[j] < 0){
                arr[j + 1] = arr[j];
                j = j - 1;
            }
            arr[j + 1] = key;
        }

    }
}
