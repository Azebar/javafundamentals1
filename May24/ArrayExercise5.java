package May24;

import java.util.Arrays;
import java.util.Random;

public class ArrayExercise5 {

    public static void main(String[] args) {
        Random random = new Random();
        int arrLength = 10;
        int[] array = createNewRandomArray(arrLength,random);
        displayArray(array);
        displayOddNumbers(array);
        displayEvenNumbers(array);
    }

    public static int[] createNewRandomArray(int length, Random random){
        int[] array = new int[length];
        for (int i = 0; i < length; i++){
            array[i] = random.nextInt(100);
        }
        return array;
    }

    public static void displayArray(int[] array){
        System.out.println(Arrays.toString(array));
    }

    public static void displayOddNumbers(int[]array){
        System.out.println("All odd numbers of array: ");
        for (int i = 0; i < array.length; i++){
            if(array[i] % 2 != 0){
                System.out.print(array[i]+ " ");
            }
        }
        System.out.println();
    }

    public static void displayEvenNumbers(int[]array){
        System.out.println("All even numbers of array: ");
        for (int i = 0; i < array.length; i++){
            if(array[i] % 2 == 0){
                System.out.print(array[i]+ " ");
            }
        }
    }

}
