package May24;

import java.util.Scanner;
import java.util.Arrays;

public class ArrayExercise3 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the length of Array1: ");
        int len1 = scanner.nextInt();
        int array1[] = getArrayFromUser(len1,scanner);

        System.out.println("Enter the length of Array2: ");
        int len2 = scanner.nextInt();
        int array2[] = getArrayFromUser(len2,scanner);

        System.out.println("Array 1: " + Arrays.toString(array1));
        System.out.println("Array 2: " + Arrays.toString(array2));

        boolean equal = true;

        if (len1 == len2){
            for (int i = 0; i <len1; i++){
                if (array1[i] != array2[i]){
                    equal = false;
                }
            }
        }else {
            equal = false;
        }
        if (equal){
            System.out.println("Arrays are equal!");
        } else {
            System.out.println("Arrays not equal!");
        }
    }

    public static int[] getArrayFromUser(int length, Scanner scanner){

        System.out.println("Enter the values of Array: ");
        int[] array = new int[length];
        for (int i = 0; i < length; i++){
            array[i] = scanner.nextInt();
        }
        return array;
    }

}
