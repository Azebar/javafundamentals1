package May24;

import java.util.Arrays;

public class ArrayExercise7 {

    public static void main(String[] args) {

        int[] arr = new int[]{-1, 10, 8, 3, 2, -7, -3};
        int rotate = 1;
        shiftRight(arr, arr.length, rotate);
        System.out.println(Arrays.toString(arr));

    }

    public static void shiftRight(int[]arr,int len, int r) {
        int temp;

        for (int i = 0; i < r; i++) {
            temp = arr[len-1];
            for (int j = len - 1; j > 0; j--){
                arr[j] = arr[j-1];
            }
            arr[0] = temp;
        }
    }
}
