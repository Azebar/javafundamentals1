package May24;

import java.util.Arrays;
import java.util.Scanner;

public class ArrayExercise4 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the length of Array1: ");
        int len1 = scanner.nextInt();
        int array1[] = getArrayFromUser(len1,scanner);
        int[] reverseArray = new int[len1];
        for (int i = array1.length-1; i >= 0; i--){
            reverseArray[array1.length -1 -i] = array1[i];
        }
        System.out.println(Arrays.toString(reverseArray));
    }

    public static int[] getArrayFromUser(int length, Scanner scanner){

        System.out.println("Enter the values of Array: ");
        int[] array = new int[length];
        for (int i = 0; i < length; i++){
            array[i] = scanner.nextInt();
        }
        return array;
    }

}
