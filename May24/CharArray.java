package May24;

import java.util.Random;

public class CharArray {

    public static void main(String[] args) {

        Random random = new Random();
        int leftLimit = 97; //a
        int rightLimit = 122; //z
        int max = 0;
        String longestCharSequence = " ";
        String[] randomArray = new String[random.nextInt(20)+1];

        for (int j = 0; j < randomArray.length; j++ ) {
            int targetLength = random.nextInt(20);
            StringBuilder buffer = new StringBuilder(targetLength);
            for (int i = 0; i < targetLength; i++) {
                int randomInt = leftLimit + (int) (random.nextFloat() * (rightLimit - leftLimit + 1));
                buffer.append((char) randomInt);
            }
            randomArray[j] = buffer.toString();
            System.out.println(randomArray[j]);
            if (randomArray[j].length() > max){
                max = randomArray[j].length();
                longestCharSequence = randomArray[j];
            }
        }

        System.out.println("The longest char sequence is: " + longestCharSequence);

    }

}
