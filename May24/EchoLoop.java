package May24;

import java.util.Scanner;

public class EchoLoop {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        while(true){
            System.out.println("Please enter some word: ");
            String word = scanner.next();
            if(word.equals("continue")){
                continue;
            }
            else if(word.equals("quit")){
                System.out.println("good bye!");
                return;
            }else{
            System.out.println(word);}

        }

    }

}