package May24;

import java.util.Random;

public class Arrays {

    public static void main(String[] args) {

        Random random = new Random();
        int[] randomArray = new int[random.nextInt(20)+1];
        int max = 0;
        for(int i = 0; i < randomArray.length; i++){
            randomArray[i] = random.nextInt(100);
            System.out.print(randomArray[i] + " ");
            if(randomArray[i] > max){
                max = randomArray[i];
            }
        }
        System.out.println("Biggest element is: " + max);
    }

}
