package May23;

import java.util.Scanner;

public class Calculator {

    public static void main(String[] args) {

        double a, b;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter first number:");
        a = scanner.nextDouble();
        System.out.println("Enter second number:");
        b = scanner.nextDouble();
        System.out.println("Choose action: +  -  *  /");
        char action = scanner.next().charAt(0);
        if(action == '+'){
            System.out.println("Result is: " + Maths.add(a, b));
        }else if(action == '-'){
            System.out.println("Result is: " + Maths.subtract(a, b));
        }else if(action == '*'){
            System.out.println("Result is: " + Maths.multiply(a, b));
        }else if(action == '/'){
            System.out.println("Result is: " + Maths.divide(a, b));
        }else {
            System.out.println("Wrong input!");
        }


    }

}
