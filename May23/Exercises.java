package May23;

import java.util.Scanner;

public class Exercises {

    public static void main(String[] args) {

        int a, b;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter first number: ");
        a = scanner.nextInt();
        System.out.println("Enter second number: ");
        b = scanner.nextInt();

        if(a > 30 || b > 30){
            System.out.println("greater");
        }if(a < 30 && b < 30){
            System.out.println("lower");
        }if(a == 30 && b == 30){
            System.out.println("equal");
        }

        int number;
        System.out.println("Enter number from 0 to 9: ");
        number = scanner.nextInt();
        switch (number){
            case 0:
                System.out.println("zero");
                break;
            case 1:
                System.out.println("one");
                break;
            case 2:
                System.out.println("two");
                break;
            case 3:
                System.out.println("three");
                break;
            case 4:
                System.out.println("four");
                break;
            case 5:
                System.out.println("five");
                break;
            case 6:
                System.out.println("six");
                break;
            case 7:
                System.out.println("seven");
                break;
            case 8:
                System.out.println("eight");
                break;
            case 9:
                System.out.println("nine");
                break;
        }

    }

}
