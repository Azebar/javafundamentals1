package May23;

import java.text.DecimalFormat;
import java.util.Scanner;

public class DivideBy {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter a number to be divided: ");
        int num = scanner.nextInt();

        int division;

        DecimalFormat decimalFormat = new DecimalFormat(("##.####"));

        do{
            System.out.println("Please enter number for dividing: ");
            division = scanner.nextInt();
            if(division != 0) {
                System.out.println(" The division is: " + decimalFormat.format(num / division));
            }
        }
        while(division != 0);{
            System.out.println(" The division is: " + decimalFormat.format(num / division));
       }

    }

}
