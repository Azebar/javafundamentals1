package May23;

public class Activities {

    protected int hunger;
    protected int energy;
    protected int happy;

    public Activities() {
        this.hunger = 0;
        this.energy = 10;
        this.happy = 10;
    }

    public void play(){
        hunger++;
        energy--;
        happy++;
    }

    public void feed(){
        hunger--;
        energy++;
        happy++;
    }

    public void sleep(){
        hunger++;
        energy++;
        happy++;
    }

    @Override
    public String toString() {
        return "hunger: " + hunger
                + " energy: " + energy
                + " happy: " + happy;
    }

}
