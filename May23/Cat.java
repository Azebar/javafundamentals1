package May23;

public class Cat extends Activities{

    @Override
    public void play(){
        hunger++;
        energy--;
        happy++;
        meow();
    }

    private void meow(){
        System.out.println("Make cat sounds!");
    }

}
