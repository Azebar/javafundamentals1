package Homework2;

public class Dog {
    private String name;
    private String breed;
    private int age;

    public Dog(String name, String breed, int age) {
        this.name = name;
        this.breed = breed;
        this.age = age;
    }

    public String getName() {
        return this.name;
    }

    public String getBreed() {
        return this.breed; }

    public int getAge() {
        return this.age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void dogTalk(){
        switch (getBreed()){
            case "Golden Retriever":
                System.out.println("Guf-Guf");
                break;
            case "Bloodhound":
                System.out.println("Buf-Buf");
                break;
            case "Border Terrier":
                System.out.println("Tuf-Tuf");
                break;
            case "Border Collie":
                System.out.println("Cuf-Cuf");
                break;
            case "West Highland White Terrier":
                System.out.println("Wuf-Wuf");
                break;
        }
    }

}
