package Homework2;

public class Cat {
    private String name;
    private String breed;
    private int age;

    public Cat(String name, String breed, int age) {
        this.name = name;
        this.breed = breed;
        this.age = age;
    }

    public String getName() {
        return this.name;
    }

    public String getBreed() {
        return this.breed; }

    public int getAge() {
        return this.age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void catTalk(){
        switch (getBreed()){
            case "Abyssinian":
                System.out.println("Mjau-Mjau");
                break;
            case "Bengal":
                System.out.println("Bjau-Bjau");
                break;
            case "Cornish Rex":
                System.out.println("Cjau-Cjau");
                break;
            case "Devon Rex":
                System.out.println("Djau-Djau");
                break;
            case "Highlander":
                System.out.println("Hjau-Hjau");
                break;
        }
    }

}
