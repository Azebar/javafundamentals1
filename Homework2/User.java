package Homework2;

public class User {
    private String name;
    private String idNumber;
    private String gender;

    public User(String name, String idNumber, String gender) {
        this.name = name;
        this.idNumber = idNumber;
        this.gender = gender;
    }

    public String getName() {
        return this.name;
    }

    public String getIdNumber() {
        return this.idNumber; }

    public String getGender() {
        return this.gender;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

}
