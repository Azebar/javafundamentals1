package Homework2;

import java.util.Scanner;

public class homework {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        User user = new User("","","");
        System.out.println("Hello! Please enter your name:");
        user.setName(scanner.nextLine());
        System.out.println("Your name is: "+ user.getName());
        System.out.println("Now please enter your ID number:");
        user.setIdNumber(scanner.nextLine());
        System.out.println("Your ID number is: "+ user.getIdNumber());
        System.out.println("Now please enter your gender:");
        user.setGender(scanner.nextLine());
        System.out.println("Your gender is: "+ user.getGender());

        System.out.println("Hello! How many dogs do you have?");
        int dogsCount = scanner.nextInt();
        scanner.nextLine();
        Dog[] dog = new Dog[dogsCount];
        Dog oldestDog = new Dog("","",0);
        if (dogsCount == 0){
            System.out.println("That's sad :(");
        }   else {
            for(int i = 0; i < dogsCount;i++){
                dog[i] = new Dog("","",0);
                System.out.println("Please enter your dog nr"+(i+1)+" name:");
                dog[i].setName(scanner.nextLine());
                System.out.println("Please enter your dog nr"+(i+1)+" breed(breed must be one from the list: Golden Retriever, Bloodhound, Border Terrier, Border Collie, West Highland White Terrier):");
                dog[i].setBreed(scanner.nextLine());
                System.out.println("Please enter your dog nr"+(i+1)+" age:");
                dog[i].setAge(scanner.nextInt());
                scanner.nextLine();
                if(dog[i].getAge()>oldestDog.getAge()){
                    oldestDog = dog[i];
                }
            }
        }

        System.out.println("Hello! How many cats do you have?");
        int catsCount = scanner.nextInt();
        scanner.nextLine();
        Cat[] cat = new Cat[catsCount];
        Cat oldestCat = new Cat("","",0);
        if (catsCount == 0){
            System.out.println("That's sad :(");
        }   else {
            for(int i = 0; i < catsCount;i++){
                cat[i] = new Cat("","",0);
                System.out.println("Please enter your cat nr"+(i+1)+" name:");
                cat[i].setName(scanner.nextLine());
                System.out.println("Please enter your cat nr"+(i+1)+" breed(breed must be one from the list: Abyssinian, Bengal, Cornish Rex, Devon Rex, Highlander):");
                cat[i].setBreed(scanner.nextLine());
                System.out.println("Please enter your cat nr"+(i+1)+" age:");
                cat[i].setAge(scanner.nextInt());
                scanner.nextLine();
                if(cat[i].getAge()>oldestCat.getAge()){
                    oldestCat = cat[i];
                }
            }
        }
        if (dogsCount > 0) {
            System.out.print("Oldest dog is: " + oldestDog.getAge() + " years old, its name is " + oldestDog.getName() + " and breed is " + oldestDog.getBreed()+".");
            System.out.println(" And it greets you with: ");
            oldestDog.dogTalk();
        } else {
            System.out.println("You don't have dogs.");
        }
        if (catsCount > 0) {
            System.out.print("Oldest cat is: " + oldestCat.getAge() + " years old, its name is " + oldestCat.getName() + " and breed is " + oldestCat.getBreed()+".");
            System.out.println(" And it greets you with: ");
            oldestCat.catTalk();
        } else {
            System.out.println("You don't have cats.");
        }
    }

}
