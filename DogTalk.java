public class DogTalk {
    private String name;
    private String breed;
    private int age;
    // creating the constructor:
    public void dogTalk(String name, String breed, int age) {
        this.name = name;
        this.breed = breed;
        this.age = age;
    }
    // getters and setters for all properties:
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getBreed() {
        return breed;
    }
    public void setBreed(String breed) {
        this.breed = breed;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    // creating talk-method, different sound for each breed:
    public void talk() {
        switch (breed) {
            case "Bulldog":
                System.out.println("Rrrauhh!");
                break;
            case "Poodle":
                System.out.println("Auh-auh-auh!");
                break;
            case "Collie":
                System.out.println("Yeeah-mauh!");
                break;
            case "Labrador":
                System.out.println("I'm here, auh!");
                break;
            case "Schnauzer":
                System.out.println("Lahv-lahv");
                break;
            default:
                System.out.println("Auh!");
        }
    }
    public static void main(String[] args) {
        // creating some dogs to test the class DogTalk:
        DogTalk dog1 = new DogTalk();
        DogTalk dog2 = new DogTalk();
        DogTalk dog3 = new DogTalk();
        DogTalk dog4 = new DogTalk();
        DogTalk dog5 = new DogTalk();
        dog1.dogTalk("Muri", "Schnauzer", 2);
        dog2.dogTalk("Tommy", "Labrador", 2);
        dog3.dogTalk("Colin", "Collie", 2);
        dog4.dogTalk("Nipi", "Poodle", 2);
        dog5.dogTalk("Papa", "Bulldog", 2);
        dog1.talk();
        dog2.talk();
        dog3.talk();
        dog4.talk();
        dog5.talk();
    }
}