public class Person {
    //access modifiers
    //int - integers - number
    //id - field name
    public int idNumber;
    public String name;

    public void setName(String name) {
        this.name = name;
    }

    public static void main(String[] args) {
        //create object, instance
        Person person = new Person();
        person.setName("Artur");
        // asigned / set a name for person object
        System.out.println(person.name);
        person.setName("Irma");
        // this person object name changed to Irma
        System.out.println(person.name);
        person.name = "Kristi";
        //another way to change name
        System.out.println(person.name);
    }
}
