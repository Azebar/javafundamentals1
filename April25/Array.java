package April25;

import java.util.Scanner;
public class Array {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("How many animals you have?");
        int animalCount = scanner.nextInt();

        Animal[] animals = new Animal[animalCount];
        for(int i = 0; i < animalCount; i++) {
            String name = "animal"+i;
            System.out.println("Tell me the age of your " + (i+1) + ". animal");
            int age = scanner.nextInt();
            if(age>5){
            // Animal animal = new Animal(name, age);
            // animals[i] = animal;
            animals[i] = new Animal(name, age);}
        }
        // average age
        int totalAge = 0;
        for (Animal animalX : animals) {
            totalAge = totalAge + animalX.getAge();
            // totalAge += animalX.getAge();
        }
        int avgAge = totalAge/animalCount;
        System.out.println("Average age of your animals is " + avgAge);
    }
}