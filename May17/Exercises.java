package May17;

public class Exercises {

    public static void main(String[] args) {

        //printPattern(5);
        //fib(10);
//        System.out.println("");
//        System.out.println("");
//        System.out.println("");
//        System.out.println("");
        stringFnc();

    }

    static void stringFnc(){
        String a = "THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG";
        System.out.println(a.toLowerCase());

        String b = "JAVA";
        for(int i = b.length() - 1; i >= 0; i--){
            System.out.print(b.charAt(i));
        }
        System.out.println();
        String c = "Programmer";
        int l = c.length()/2;
        System.out.println(c.substring(0,l));

        String j = "Java";
        String f = "Fundamentals";
        int j1 = j.length();
        int f1 = f.length();
        System.out.println(j.substring(1,j1) + f.substring(1,f1));
    }

    static void fib(int n){           //Fibonacci

        int a = 0, b = 1, c = 0;

        for(int i = 1; i <= n; i++){
            System.out.print(a + " ");

            c = a + b;
            a = b;
            b = c;
        }

    }


   static void printPattern(int n){         //Pyramid

        for(int i = 0; i < n; i++) {
            for(int j = n - i; j > 1; j--) {
                System.out.print(" ");
            }
            for (int j = 0; j <= 2 * i; j++) {
                System.out.print(i + 1);
            }
            System.out.println();
        }
    }

}
