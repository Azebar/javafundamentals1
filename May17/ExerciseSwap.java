package May17;

public class ExerciseSwap {

    public static void main(String[] args) {

        int a = 12, b = 9;
        swapIt(a,b);

    }

    public static void swapIt(int a, int b){
//        int temp = 0;
//        temp = a;
//        a = b;
//        b = temp;
        a = a + b;
        b = a - b;
        a = a - b;

        System.out.println("a = " + a + " b = " + b);
    }

}
