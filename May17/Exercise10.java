package May17;

public class Exercise10 {

    public static void main(String[] args) {

        String test = "Aa kiu, I swd skieo 2387. GH kiu: sieo?? 25.33";
        countEverything(test);

    }

    private static void countEverything(String test){
        char[] chars = test.toCharArray();
        int letters = 0;
        int spaces = 0;
        int numbers = 0;
        int other = 0;

        for(int i = 0; i < chars.length; i++){
            if(Character.isLetter(chars[i])){
                letters++;
            }else if(Character.isSpaceChar(chars[i])){
                spaces++;
            }else if(Character.isDigit(chars[i])){
                numbers++;
            }else {
                other++;
            }
        }
        System.out.println("Letters: " + letters + " , spaces: " + spaces + " , numbers: " + numbers + " , others: " + other);
    }

}
