package May17;

public class ArrayValuesSum {

    public static void main(String[] args) {

        int sum = 0;
        int[] a = new int[]{1, 7, 3, 10, 9};
//        System.out.println(sum(sum,a));
//        System.out.println(avg(sum(sum,a), a.length));
//        getOdd(a);
//        int[] newArray = insert(a,4,2);
//        for(int n:newArray){
//            System.out.print(n + " ");
//        }
        secondLargestElement(a);
    }

    private static void secondLargestElement(int[] a){
        int max = 0;
        int secondLargest = 0;
        for(int i = 0; i < a.length; i++){
            if(a[i] > max){
                max = a[i];
            }
        }
        for(int i = 0; i < a.length; i++){
            if(a[i] > secondLargest && a[i] < max){
                secondLargest = a[i];
            }
        }
        System.out.println(secondLargest);
    }

    private static int sum(int sum, int[] a){

        for(int i = 0; i < a.length; i++){
            sum += a[i];
        }
        return sum;

    }
    private static int avg(int sum, int len){
        return sum/len;
    }

    private static void getOdd(int[] a){
        for(int i = 0; i < a.length; i++){
            if(a[i] % 2 == 0){
                System.out.println(a[i]);
            }
        }
    }

    private static int[] insert(int[] a, int value, int position){
        int[] n_array = new int[a.length+1];
        for(int i = 0; i < position; i++){
            n_array[i] = a[i];
        }
        n_array[position] = value;
        for(int i = position; i < a.length; i++){
            n_array[i+1] = a[i];
        }
        return n_array;
    }

}
