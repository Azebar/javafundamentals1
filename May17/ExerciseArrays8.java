package May17;

public class ExerciseArrays8 {

    public static void main(String[] args) {

        int[] array = new int[]{1, 7, 3, 7, 10, 1, 9};
        findDuplicateValues(array);

    }

    private static void findDuplicateValues(int[] array){
        for(int i = 0; i < array.length - 1; i++){
            for(int j = i + 1; j < array.length; j++) {
                if (array[i] == array[j] && i != j){
                    System.out.print(array[i]);
                }
            }
        }
    }

}
